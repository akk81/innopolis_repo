package ru.kulikov.taskOOP;

public class Cashier {
    private String name;
    private int inn;

    public Cashier(String name, int inn) {
        this.name = name;
        this.inn = inn;
    }

    public void sellItem (Item item, SimplePos pos, int sum){
        pos.addItem(item);
        pos.addPayment(sum);
    }
}
