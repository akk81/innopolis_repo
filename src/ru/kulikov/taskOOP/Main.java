package ru.kulikov.taskOOP;

public class Main {
    public static void main(String[] args) {
        SimplePos pos = new PosWithFR("NSR", "POS M-R-P", 1);
        Cashier cashier = new Cashier("Галина Ивановна", 1234567891);
        Item item = new Item("Caca-cola 1l", 1846, 0);
        item.setprice(100);
        cashier.sellItem(item, pos, 500);
    }
}
