package ru.kulikov.taskOOP;

public class PosWithFR extends SimplePos {

    public PosWithFR(String manufacturer, String model, int number) {
        super(manufacturer, model, number);
    }

    private void makeReceipt(){};
    private void registerBonInFr(){};
    private void printReceipt(){};

    @Override
    public void addItem(Item item){
        makeReceipt();
        System.out.println("Добавить товар в чек");
    }

    @Override
    public void addPayment(int sum){
        System.out.println("Добавить оплату в чек");
        registerBonInFr();
        printReceipt();
    }
}
