package ru.kulikov.taskOOP;

public class Item {
    private String name;
    private int skuCode;
    private float price;

    public Item(String name, int skuCode, int price) {
        this.name = name;
        this.skuCode = skuCode;
        this.price = price;
    }

    public void setprice (int newprice){
        this.price = newprice;
    }

}
