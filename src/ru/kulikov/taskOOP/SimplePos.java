package ru.kulikov.taskOOP;

public abstract class SimplePos {
    private String manufacturer;
    private String model;
    private int number;

    public SimplePos(String manufacturer, String model, int number){
        this.manufacturer = manufacturer;
        this.model = model;
        this.number = number;
    }

    public abstract void addItem(Item item);
    public abstract void addPayment(int sum);

}
