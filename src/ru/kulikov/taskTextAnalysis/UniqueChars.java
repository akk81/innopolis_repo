package ru.kulikov.taskTextAnalysis;

import java.util.Map;
import java.util.TreeMap;

public class UniqueChars {

    private String text;
    private Map<Character, Double> map = new TreeMap<>();

    //public String getText() {return "TODO  - realize method calculate and change method getText()";}
    public String getText() {
        String string = "";
        for (Map.Entry<Character, Double> element : map.entrySet()) {
            string += "'" + element.getKey() + "'" + " - " + element.getValue() + "%\n";
        }
        return string;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void calculate() {
        Character a;
        Double qty;
        map.clear();
        for (int i = 0; i < text.length(); i++) {
            a = text.charAt(i);
            qty = map.get(a);
            map.put(a, qty == null ? 1 : qty + 1);
        }
        for (Map.Entry<Character, Double> element : map.entrySet()) {
            a = element.getKey();
            qty = element.getValue() * 100 / text.length();
            qty = Math.round(qty * 100) / 100.0;  //округление
            map.put(a, qty);
        }
    }
}
