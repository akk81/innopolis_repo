package ru.kulikov.task4;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Number[] array = {1, 2, 3.1, 3.2, 3.3, 3.4, 7, 8, 9, 10, 1, 10, 1};
        MathBox<Number> myMathBox = new MathBox<>(array);

        //Integer[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 10, 1};
        //MathBox<Integer> myMathBox = new MathBox<>(array);

        System.out.println("Содержимое коллекции Set");
        System.out.println(myMathBox.toString());
        System.out.println("Добавляем новое значение");
        myMathBox.add(33333.33);
        System.out.println(myMathBox.toString());
        System.out.println("Удаляем это значение");
        myMathBox.remove(33333.33);
        System.out.println(myMathBox.toString());
        System.out.println("Деление всех элементов коллекции");
        myMathBox.getSplit(2);
        System.out.println(myMathBox.toString());
        System.out.println("Умножение всех элементов коллекции");
        myMathBox.getMultiplied(2);
        System.out.println(myMathBox.toString());
        System.out.println("Сумма всех элементов = " + myMathBox.getTotal());
        System.out.println("Среднее значение элементов = " + myMathBox.getAverage());
        System.out.println("Максимальный элемент = " + myMathBox.getMax());
        System.out.println("Минимальный элемент = " + myMathBox.getMin());
        System.out.println("Коллекция Map (ключ Integer)");
        System.out.println(myMathBox.getMap());




    }
}
