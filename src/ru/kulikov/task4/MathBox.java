package ru.kulikov.task4;

import java.util.*;

public class MathBox<E extends Number> {

    private Set<Double> mySet;

    public MathBox(E[] array) {
        //mySet = (Set<Double>) new HashSet<E>(Arrays.asList(array));
        mySet = new HashSet<>();
        for (E element: array) {
            mySet.add(element.doubleValue());
        }
    }

    public void add(Double number){
        mySet.add(number);
    }

    public void remove(Double number){
        mySet.remove(number);
    }

    public double getTotal(){
        double total = 0;
        for (Double element: mySet) {
            total += element;
        }
        return total;
    }

    public void getSplit(int splitter){
        Set<Double> tempSet = new HashSet<>();
        double temp;
        for (Double element: mySet) {
            temp = element;
            tempSet.add(temp / splitter);
        }
        mySet.clear();
        mySet.addAll(tempSet);
    }

    public double getAverage(){
        return this.getTotal() / mySet.size();
    }

    public void getMultiplied(int multiplicator){
        Set<Double> tempSet = new HashSet<>();
        double temp;
        for (Double element: mySet) {
            temp = element;
            tempSet.add(temp * multiplicator);
        }
        mySet.clear();
        mySet.addAll(tempSet);
    }

    public double getMax(){
        double maxNumber = mySet.iterator().next();
        for (Double element: mySet) {
            if (maxNumber < element) {maxNumber = element;}
        }
        return maxNumber;
    }

    public double getMin(){
        double minNumber = mySet.iterator().next();
        for (Double element: mySet) {
            if (minNumber > element) {minNumber = element;}
        }
        return minNumber;
    }

    public Map<Integer, String> getMap(){
        Map<Integer, String> map = new HashMap<>();
        Set<Integer> tempSet = new HashSet<>();
        for (Double element: mySet) {
            tempSet.add(element.intValue());
        }
        for (Integer element: tempSet) {
            map.put(element, element.toString());
        }
        /*for (Double element: mySet) {          // for Map<Double, String>
            map.put(element, element.toString());
        }*/
        return map;
    }


    @Override
    public String toString(){
        return mySet.toString();
    }
}
