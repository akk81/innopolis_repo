package ru.kulikov.practice2.reverse;

import ru.kulikov.practice2.structure.StackX;

public class MassiveRev {
    private static int[] myMassive;
    private static int size;

    public MassiveRev(int size) {
        this.size = size;
        myMassive = new int[size];
    }

    public static void fill() {
        for (int i = 0; i < size; i++) {
            myMassive[i] = i;
        }
    }

    public void display() {
        for (int i = 0; i < size; i++) {
            System.out.printf("%2d",myMassive[i]);
        }
    }

    public void reverse() {
        StackX myStack = new StackX(size);
        for (int i = 0; i < size; i++) {
            myStack.push(myMassive[i]);
        }
        for (int i = 0; i < size; i++) {
            myMassive[i] = myStack.pop();
        }

     /* int temp;
        for (int i = 0; i < size / 2; i++) {
            temp = myMassive[i];
            myMassive[i] = myMassive[size - 1 - i];
            myMassive[size - 1 - i] = temp;
        } */
    }

}
