package ru.kulikov.practice2.reverse;

import static ru.kulikov.practice2.reverse.MassiveRev.fill;

public class MainApp {
    public static void main(String[] args) {
        MassiveRev massiveRev = new MassiveRev(10);
        fill();
        massiveRev.display();
        System.out.println();
        massiveRev.reverse();
        massiveRev.display();
    }
}
