package ru.kulikov.practice2.structure;

public class MainApp {
    public static void main(String[] args) {
        StackX myStack = new StackX(10);
        myStack.push(1);
        myStack.push(-9);
        myStack.display();
        myStack.peek();
        System.out.println("Минимальное значение стека = " + myStack.getMin());
        System.out.println(myStack.pop());
        myStack.display();
    }
}
