package ru.kulikov.practice2.structure;

public class StackX {
    private int[] array;
    private int size;
    private int pos;

    public StackX(int size) {
        this.size = size;
        this.pos = -1;
        array = new int[this.size];

    }

    public void peek() {
        System.out.println("array[" + pos + "] = " + array[pos]);
    }

    public int pop() {
        return pos > -1 ? array[pos--] : 0;
        /*if (pos > -1) {
            return array[pos--];
        } return -1;*/

    }

    public void push(int elment) {
        if (!isFull()) {
            array[++pos] = elment;
        }
    }

    public void display() {
        for (int i = pos; i >= 0; i--) {
            System.out.println("array[" + i + "] = " + array[i]);
        }
    }

    public boolean isEmpty() {
        return (pos == -1);
    }

    public boolean isFull() {
        return (pos == size -1);
    }

    public int getMin() {
        int minValue = array[0];
        for (int i = 0; i <= pos; i++) {
            minValue = Math.min(minValue, array[i]);
        }
        return minValue;
    }
}
