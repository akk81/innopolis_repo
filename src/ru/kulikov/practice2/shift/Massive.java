package ru.kulikov.practice2.shift;

public class Massive {
    private int[][] myMassive;
    private int size;

    public Massive(int size) {
        myMassive = new int[size][size];
        this.size = size;
    }

    public void toRight() {
        for (int i = size - 1; i >= 0; i--) {
            for (int j = size - 1; j >= 1; j--) {
                myMassive[i][j] = myMassive[i][j - 1];
            }
            myMassive[i][0] = 0;
        }
    }

    public void toLeft() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size - 1; j++) {
                myMassive[i][j] = myMassive[i][j + 1];
            }
            myMassive[i][size - 1] = 0;
        }
    }

    public void fill() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                myMassive[i][j] = j;
            }
        }
    }

    public void display() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.printf("%2d",myMassive[i][j]);
            }
            System.out.println();
        }
    }
}
