package ru.kulikov.practice2.shift;

public class MainApp {
    public static void main(String[] args) {
        Massive massive = new Massive(10);
        massive.fill();
        massive.display();
        massive.toLeft();
        System.out.println();
        massive.display();
        massive.toRight();
        System.out.println();
        massive.display();
    }
}
