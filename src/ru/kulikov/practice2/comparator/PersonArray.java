package ru.kulikov.practice2.comparator;

import java.util.Arrays;

public class PersonArray {
    private static Person[] array;

    public PersonArray(Person[] array) {
        this.array = array;
    }

    public static void main(String[] args) {
        Person person1 = new Person("Bill", 28);
        Person person2 = new Person("Tom", 27);
        Person person3 = new Person("Ann", 31);
        Person person4 = new Person("Tom", 21);

        array = new Person[]{person1, person2, person3, person4};
        System.out.println(Arrays.toString(array));
        //Arrays.sort(array);
        Arrays.sort(array, new PersonComparator());
        System.out.println(Arrays.toString(array));
        Arrays.sort(array, new PersonAgeComparator());
        System.out.println(Arrays.toString(array));
    }
}
