package ru.kulikov.practice5.task3;

import java.io.*;
import java.util.*;

public class Main {
    //private static final Logger logger = Logger.getLogger(Main.class.getName());
    //private static final String FILENAME = "C:\\Users\\Andrey.Kulikov\\Desktop\\note.txt";
    private static final String FILENAME = "note.txt";

    public static void main(String[] args) {
        File file = new File(FILENAME);
        Set<String> stringList = new HashSet<>();
        try (Scanner scanner = new Scanner(file)) {
            //scanner.useDelimiter("\\ ");
            scanner.useDelimiter("\\p{P}?[ \\t\\n\\r]+");
            while (scanner.hasNext()) {
                String str = scanner.next();
                stringList.add(str.replace(".", "").toLowerCase());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(stringList.toString());

    }



}
