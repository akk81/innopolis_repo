package ru.kulikov.practice5.task5;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    public static void main(String[] args) throws IOException {
        InetAddress addr = InetAddress.getByName("127.0.0.1");
        //System.out.println(addr);
        Socket socket = new Socket(addr, Server.PORT);
       // System.out.println(socket);
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
            Scanner console = new Scanner(System.in);
            while (true) {
                String vol = console.next();
                out.println(vol);
                if (vol.equals("END"))
                    break;
                String str = in.readLine();
                System.out.println("Server say: " + str);
            }

        } finally {
            socket.close();
        }
    }
}
