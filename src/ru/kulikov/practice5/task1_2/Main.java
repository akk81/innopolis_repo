package ru.kulikov.practice5.task1_2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.logging.Logger;

public class Main {
    private static final Logger logger = Logger.getLogger(Main.class.getName());
    private static final String FILENAME = "note.txt";

    public static void main(String[] args) {
        File file = new File(FILENAME);
        List<String> stringList = new ArrayList<>();
        try (Scanner scanner = new Scanner(file)) {
            scanner.useDelimiter("\\. ");
            while (scanner.hasNext()) {
                String str = scanner.next();
                stringList.add(str);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        logger.info(stringList.toString());

        Collections.sort(stringList);

        logger.info(stringList.toString());

    }



}
