package ru.kulikov.practice5.task4;

import java.io.*;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String fileName = "obj.txt";

        /**
         * тестовый класс и инициализирующим конструктором
         */
        class Test {
            String string;
            long aLong;
            double aDouble;

            public Test(String string, long aLong, double aDouble) {
                this.string = string;
                this.aLong = aLong;
                this.aDouble = aDouble;
            }

            @Override
            public String toString() {
                return "Test{" +
                        "string='" + string + '\'' +
                        ", aLong=" + aLong +
                        ", aDouble=" + aDouble +
                        '}';
            }
        }

        Test test = new Test("Test", 100, 15.5);

        Main util = new Main();
        //util.saveObjToFile(test, fileName);

        util.loadObjFromFile(test, fileName);
        System.out.println(test);

    }

    public void saveObjToFile(Object obj, String fileName) {
        Class aClass = obj.getClass();
        HashMap<String, String> propertys = new HashMap();
        propertys.put("class", aClass.getName());
        for (Field declaredField : aClass.getDeclaredFields()) {
            try {
                propertys.put(declaredField.getName(), String.valueOf(declaredField.get(obj)));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        try (PrintWriter out = new PrintWriter(fileName)) {
            propertys.forEach((s, s2) -> {
                out.println(s + "=" + s2);
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void loadObjFromFile(Object obj, String fileName) {

        HashMap<String, String> propertys = new HashMap<>();

        File file = new File(fileName);
        try {
            Scanner scanner = new Scanner(file);
            scanner.useDelimiter("\r\n");
            while (scanner.hasNext()) {
                String line = scanner.next();
                String name = line.substring(0, line.indexOf('='));
                String value = line.substring(line.indexOf('=') + 1);
//                System.out.println(name);
//                System.out.println(value);
                propertys.put(name, value);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (!obj.getClass().getName().equals(propertys.get("class"))) {
            System.out.println("wrong class");
            return;
        }
        propertys.forEach((s, s2) -> {
            if (!s.equals("class")) {
                try {
                    Field aField = obj.getClass().getDeclaredField(s);

                    String aFieldType = aField.getGenericType().getTypeName();
                    if ("java.lang.String".equals(aFieldType)) {
                        aField.set(obj, s2);
                    } else if ("long".equals(aFieldType)) {
                        aField.set(obj, Long.parseLong(s2));
                    } else if ("double".equals(aFieldType)) {
                        aField.set(obj, Double.parseDouble(s2));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
