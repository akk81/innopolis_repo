package ru.kulikov.practice4.task5;

public class Thread2 extends Thread{
    private Object resource1;
    private Object resource2;

    public Thread2(Object resource1, Object resource2) {
        this.resource1 = resource1;
        this.resource2 = resource2;
    }

    @Override
    public void run() {
        synchronized (resource2){
            System.out.println("resource2 locked by Thread2");
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (resource1){
                System.out.println("resource1 locked by Thread2");
            }
        }
    }
}
