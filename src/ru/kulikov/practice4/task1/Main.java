package ru.kulikov.practice4.task1;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Thread> threadList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            threadList.add(new MyThread(i));
            threadList.get(i).start();
        }

        for (Thread element : threadList) {
            try {
                element.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Основной поток завершился.");
    }
}
