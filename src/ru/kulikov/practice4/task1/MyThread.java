package ru.kulikov.practice4.task1;

public class MyThread extends Thread {

    int number;

    public MyThread(int number) {
        this.number = number;
    }

    @Override
    public void run() {
        System.out.println("Поток " + this.number + " завершился.");
    }
}
