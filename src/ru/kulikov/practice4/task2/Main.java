package ru.kulikov.practice4.task2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Counter counter = new Counter();
        List<Thread> threadList = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            threadList.add(new MyThread(i, counter));
            threadList.get(i).start();
        }

        Scanner scanner = new Scanner(System.in);
        if (scanner.hasNext()) {
            for (Thread thread : threadList) {
                thread.interrupt();
            }

        }

        System.out.println("Summ " + counter.get());
    }
}
