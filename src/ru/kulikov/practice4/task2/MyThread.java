package ru.kulikov.practice4.task2;

public class MyThread extends Thread {

    int number;
    Counter count;

    public MyThread(int number, Counter count) {
        this.number = number;
        this.count = count;
    }

    @Override
    public void run() {
        System.out.println("Thread " + this.number + " started");
        while (!this.isInterrupted()){

            try {
                count.add();
                this.currentThread().sleep(100);
            } catch (InterruptedException e) {
                this.currentThread().interrupt();

            }
        }
    }
}
