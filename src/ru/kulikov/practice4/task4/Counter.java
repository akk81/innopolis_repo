package ru.kulikov.practice4.task4;

public class Counter {
    private static int count;

    public Counter() {
        this.count = 0;
    }

    public static void add() {
        count += 1;
    }

    public int get () {
        return count;
    }
}
