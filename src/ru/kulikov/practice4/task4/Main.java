package ru.kulikov.practice4.task4;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        List<Thread> threadList = new ArrayList<>();
        Counter counter = new Counter();
        ReentrantLock lock = new ReentrantLock();
        Condition condition = lock.newCondition();

        threadList.add(new Chronometr(counter, lock, condition));
        threadList.add(new Message(counter, 5, lock, condition));
        threadList.add(new Message(counter, 7, lock, condition));

        for (Thread thread : threadList) {
            thread.start();
        }

        Thread.sleep(100000L);

        for (Thread thread : threadList) {
            thread.interrupt();
        }

        }
}
