package ru.kulikov.practice4.task4;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class Chronometr extends Thread{
    private Counter count;
    private Lock lock;
    private Condition condition;

    public Chronometr(Counter count, Lock lock, Condition condition) {
        this.count = count;
        this.lock = lock;
        this.condition = condition;
    }

    @Override
    public void run() {
        System.out.println("Chronometr started");
        while (!this.isInterrupted()){
            try {
                this.currentThread().sleep(1000);
                lock.lock();
                count.add();
                System.out.println("Прошло " + count.get() + " секунд");
                condition.signalAll();
                lock.unlock();
            } catch (InterruptedException e) {
                this.currentThread().interrupt();
            }

        }

    }
}
