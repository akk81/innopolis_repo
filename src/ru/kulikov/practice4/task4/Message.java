package ru.kulikov.practice4.task4;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class Message extends Thread {
    private Counter count;
    private int period;
    private Lock lock;
    private Condition condition;

    public Message(Counter count, int period, Lock lock, Condition condition) {
        this.count = count;
        this.period = period;
        this.lock = lock;
        this.condition = condition;
    }

    @Override
    public void run() {
        while (!this.isInterrupted()){
            lock.lock();
            try {
                condition.await();
            } catch (InterruptedException e) {
                this.currentThread().interrupt();
            }
            if (count.get()%period == 0){
                System.out.println("Сообщение каждые " + period + " секунд");
            }
            lock.unlock();
            }
    }
}
