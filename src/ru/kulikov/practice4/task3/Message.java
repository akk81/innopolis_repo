package ru.kulikov.practice4.task3;

public class Message extends Thread {
    Counter count;
    int period;

    public Message(Counter count, int period) {
        this.count = count;
        this.period = period;
    }

    @Override
    public void run() {
        while (!this.isInterrupted()){
            try {
                synchronized (count) {
                    count.wait();
                }
            } catch (InterruptedException e) {
                this.currentThread().interrupt();
            }
            if (count.get()%period == 0){
                System.out.println("Сообщение каждые " + period + " секунд");
            }
                    }
    }
}
