package ru.kulikov.practice4.task3;

public class Chronometr extends Thread{
    Counter count;

    public Chronometr(Counter count) {
        this.count = count;
    }

    @Override
    public void run() {
        System.out.println("Chronometr started");
        while (!this.isInterrupted()){
            try {
                this.currentThread().sleep(1000);
            } catch (InterruptedException e) {
                this.currentThread().interrupt();
            }
            synchronized (count) {
                count.add();
                System.out.println("Прошло " + count.get() + " секунд");
                count.notifyAll();
            }
        }

    }
}
