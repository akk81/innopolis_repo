package ru.kulikov.practice4.task3;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        List<Thread> threadList = new ArrayList<>();
        Counter counter = new Counter();

        threadList.add(new Chronometr(counter));
        threadList.add(new Message(counter, 5));
        threadList.add(new Message(counter, 7));

        for (Thread thread : threadList) {
            thread.start();
        }

        Thread.sleep(10000L);

        for (Thread thread : threadList) {
            thread.interrupt();
        }

        }
}
