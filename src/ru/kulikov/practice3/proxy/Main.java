package ru.kulikov.practice3.proxy;

import java.lang.reflect.Proxy;

public class Main {
    public static void main(String[] args) {
        Trainer mikhail = new JavaTrainer();
        //TrainingCenter trainingCenter = new TrainingCenter(mikhail);
        AnotherTrainingCenter anotherTrainingCenter = new AnotherTrainingCenter(mikhail);

        Trainer stc = (Trainer) Proxy.newProxyInstance(
                AnotherTrainingCenter.class.getClassLoader(),
                new Class[]{Trainer.class},
                anotherTrainingCenter);
        System.out.println("Without proxy:");
        mikhail.eat("mango");
        mikhail.teach("Java", "complicated");
        mikhail.talk();
        System.out.println("With proxy:");
        stc.teach("Java", "complicated");
        stc.eat("mango");
        stc.talk();

/*        Trainer stc = (Trainer) Proxy.newProxyInstance(
                TrainingCenter.class.getClassLoader(),
                new Class[]{Trainer.class},
                trainingCenter);
        System.out.println("Without proxy:");
        mikhail.eat();
        mikhail.teach();
        System.out.println("With proxy:");
        stc.teach();
        stc.eat();
        stc.talk();*/
    }
}
