package ru.kulikov.practice3.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class AnotherTrainingCenter implements InvocationHandler {
    private Trainer trainer;

    public AnotherTrainingCenter(Trainer trainer) {
        this.trainer = trainer;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //method.getAnnotation()
        System.out.println("Попытка запуска метода " + method.getName());
        String argsString = "";
        try {
            for (int i = 0; i < args.length; i++) {
                argsString += args[i].toString() + " ";
            }
            System.out.println("Аргументы метода " + argsString);
        } catch (NullPointerException a) {
            System.out.println("Аргументы отсутствуют");
        }
        return null;
    }
}
