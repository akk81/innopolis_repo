package ru.kulikov.practice3.proxy;

public interface Trainer {
    public void teach(String subject, String difficulty);

    public void eat(String food);

    public void talk();
}
