package ru.kulikov.practice3.logged;

@Logged
public class JavaTrainer implements Trainer {
    @Override
    public void teach(String subject, String difficult) {
        System.out.println(subject + " is really " + difficult);
    }

    @Override
    public void eat(String food) {
        System.out.println("I like to eat " + food);
    }

    @Override
    public void talk() {
        System.out.println("Where is your homework?");
    }
}