package ru.kulikov.practice3.logged;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class TrainingCenter implements InvocationHandler {
    private Trainer trainer;

    public TrainingCenter(Trainer trainer) {
        this.trainer = trainer;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        if (trainer.getClass().isAnnotationPresent(Logged.class)) {
            long startTime = System.currentTimeMillis();
            Object result = method.invoke(trainer, args);
            long endTime = System.currentTimeMillis();
            long duration = endTime - startTime;
            System.out.println("Время начала - " + startTime + ", Время окончания - " + endTime
                               + ", Длительность - " + duration);
            return result;
        }
        return method.invoke(trainer, args);

    }
}
