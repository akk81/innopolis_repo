package ru.kulikov.practice3.logged;

import java.lang.reflect.Proxy;

public class Main {
    public static void main(String[] args) {
        Trainer mikhail = new JavaTrainer();
        TrainingCenter trainingCenter = new TrainingCenter(mikhail);

        Trainer stc = (Trainer) Proxy.newProxyInstance(
                TrainingCenter.class.getClassLoader(),
                new Class[]{Trainer.class},
                trainingCenter);

        System.out.println("Without proxy:");
        mikhail.eat("mango");
        mikhail.teach("Java", "complicated");
        mikhail.talk();
        System.out.println("With proxy:");
        stc.eat("mango");
        stc.teach("Java", "complicated");
        stc.talk();
    }
}
