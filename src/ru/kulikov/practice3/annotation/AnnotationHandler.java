package ru.kulikov.practice3.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AnnotationHandler {

    public void printAnnotation (Object object) {
        HashMap<String, List<Annotation>> annotationMap = getAnnotation(object);
        for (Map.Entry<String, List<Annotation>> element: annotationMap.entrySet()) {
            String key = element.getKey();
            List<Annotation> value = element.getValue();

            System.out.println("Метод: " + key);
            for (Annotation annotation : value) {
            System.out.println("Аннотация: " + annotation.annotationType().getName());
            }
        }
    }

    private HashMap<String, List<Annotation>> getAnnotation (Object object) {
        HashMap<String, List<Annotation>> annotationsMap = new HashMap<>();
        annotationsMap.put("Class", Arrays.asList(object.getClass().getAnnotations()));

        for (Method method: object.getClass().getDeclaredMethods()){
            annotationsMap.put(method.getName(), Arrays.asList(method.getAnnotations()));
        }
        return annotationsMap;
    }

}
