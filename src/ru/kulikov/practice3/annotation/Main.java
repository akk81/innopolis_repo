package ru.kulikov.practice3.annotation;

public class Main {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        Capucin abou = new Capucin(50);
        AnnotationHandler myAnnotation = new AnnotationHandler();

        myAnnotation.printAnnotation(abou);

    }
}
