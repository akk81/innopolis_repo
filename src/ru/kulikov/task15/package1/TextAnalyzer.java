package ru.kulikov.task15.package1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class TextAnalyzer {

    private Set<String> stringList;

    public TextAnalyzer() {
        this.stringList = new TreeSet<>();
    }

    public void saveWordToFile (String inFile, String outFile) {
        stringList = splitIntoWords(inFile);
        saveStringSetToFile(stringList, outFile);
    }

    private Set<String> splitIntoWords (String inFile) {
        File file = new File(inFile);
        try (Scanner scanner = new Scanner(file)) {
            scanner.useDelimiter("\\p{P}?[ \\t\\n\\r]+");
            while (scanner.hasNext()) {
                String str = scanner.next();
                stringList.add(str.replace(".", "").toLowerCase());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return stringList;
    }

    private void saveStringSetToFile (Set<String> stringList, String outFile) {
        try (PrintWriter out = new PrintWriter(outFile)) {
            for (String word: stringList) {
                out.println(word);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
