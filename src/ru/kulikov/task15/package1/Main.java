package ru.kulikov.task15.package1;

public class Main {

    private static final String INPUTFILENAME = "note.txt";
    private static final String OUTPUTFILENAME = "note_out.txt";

    public static void main(String[] args) {

        TextAnalyzer textAnalyzer = new TextAnalyzer();
        textAnalyzer.saveWordToFile(INPUTFILENAME, OUTPUTFILENAME);

    }

}
