package ru.kulikov.task15.package2;

public class SimpleClass {

    String stringvalue;
    int intvalue;
    double doublevalue;
    boolean booleanvalue;

    public SimpleClass(String stringvalue, int intvalue, double doublevalue, boolean booleanvalue) {
        this.stringvalue = stringvalue;
        this.intvalue = intvalue;
        this.doublevalue = doublevalue;
        this.booleanvalue = booleanvalue;
    }

    public SimpleClass() {
    }

    @Override
    public String toString() {
        return "SimpleClass{" +
                "stringvalue='" + stringvalue + '\'' +
                ", intvalue=" + intvalue +
                ", doublevalue=" + doublevalue +
                ", booleanvalue=" + booleanvalue +
                '}';
    }
}
