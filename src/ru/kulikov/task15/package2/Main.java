package ru.kulikov.task15.package2;


public class Main {

    private static final String FILENAME = "object.txt";

    public static void main(String[] args) {
        SimpleClass object1 = new SimpleClass("Object1", 10, 18.87, true);
        ObjectUtil.saveObjectToFile(object1, FILENAME);

        SimpleClass object2 = new SimpleClass();
        ObjectUtil.restoreObjectFromFile(object2, FILENAME);
        System.out.println(object2);






    }
}
