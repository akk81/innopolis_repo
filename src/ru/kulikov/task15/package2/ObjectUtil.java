package ru.kulikov.task15.package2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class ObjectUtil {

    private ObjectUtil() {
    }

    public static void saveObjectToFile (Object obj, String outFile)  {
        saveToFile(fieldsListPrepare(obj), outFile);
    }

    public static void restoreObjectFromFile (Object obj, String inFile)  {
        restoreFromFile(obj, readFromFile(inFile));
    }


    private static List<String> fieldsListPrepare (Object obj) {
        List<String> fieldsList = new ArrayList<>();
        Class<SimpleClass> clazz = (Class<SimpleClass>) obj.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            try {
                fieldsList.add(field.getName() + "," + field.getType() + "," + String.valueOf(field.get(obj)));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return fieldsList;
    }

    private static void saveToFile (List<String> stringList, String outFile) {
        try (PrintWriter out = new PrintWriter(outFile)) {
            for (String word: stringList) {
                out.println(word);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static List<String> readFromFile (String inFile) {
        File file = new File(inFile);
        List<String> stringList = new ArrayList<>();
        try (Scanner scanner = new Scanner(file)) {
            scanner.useDelimiter("\r\n");
            while (scanner.hasNext()) {
                String str = scanner.next();
                stringList.add(str);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return stringList;
    }

    private static void restoreFromFile (Object obj, List<String> properties) {
        for (String string: properties) {
            String[] property = string.split(",");
            String propertyName = property[0];
            String propertyType = property[1];
            String propertyValue = property[2];
            try {
                Field field = obj.getClass().getDeclaredField(propertyName);
                switch (propertyType) {
                    case ("class java.lang.String"): field.set(obj, propertyValue);
                        break;
                    case ("int"): field.set(obj, Integer.parseInt(propertyValue));
                        break;
                    case ("double"): field.set(obj, Double.parseDouble(propertyValue));
                        break;
                    case ("boolean"): field.set(obj, Boolean.parseBoolean(propertyValue));
                        break;
                }
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }


        }

    }



}