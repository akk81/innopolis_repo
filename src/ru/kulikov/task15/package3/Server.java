package ru.kulikov.task15.package3;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static final int PORT = 8080;

    public static void main(String[] args) throws IOException {
        ServerSocket sSocket = new ServerSocket(PORT);
        Socket socket = sSocket.accept();
        Chat chat = new Chat(socket, "Client");
        chat.startConnection();
    }
}
