package ru.kulikov.task15.package3;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Chat {

    private Socket socket;
    private String source;

    public Chat(Socket socket, String source) {
        this.socket = socket;
        this.source = source;
    }

    public void startConnection () throws IOException {
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true)) {
            Receiver receiver = new Receiver(socket, source);
            receiver.setDaemon(true);
            receiver.start();
            Scanner console = new Scanner(System.in);
            while (true) {
                String vol = console.next();
                out.println(vol);
                if (vol.equals("end"))
                    break;
            }
        }
    }
}
