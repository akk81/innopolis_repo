package ru.kulikov.task15.package3;

import java.io.IOException;
import java.net.Socket;

public class Client {

    private static Socket socket;

    public static void main(String[] args) throws IOException {
        socket = new Socket("localhost", Server.PORT);
        Chat chat = new Chat(socket, "Server");
        chat.startConnection();
    }
}
