package ru.kulikov.task15.package3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Receiver extends Thread {

    private Socket socket;
    private String source;

    public Receiver(Socket socket, String source) {
        this.socket = socket;
        this.source = source;
    }

    @Override
    public void run() {
        try (BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()))){
            while (true) {
                String str = in.readLine();
                if (str.equals("end"))
                    break;
                System.out.println(source + " said: "+str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
