package ru.kulikov.task8;

import java.lang.reflect.Proxy;

public class Main {

    public static void main(String[] args) {
        Number[] array = {1, 2, 3.1, 3.2, 3.3, 3.4, 7, 8, 9, 10, 1, 10, 1};
        MathBox myMathBox = new SetMathBox(array);
        SetMathBoxHandler mathBoxHandler = new SetMathBoxHandler(myMathBox);

        MathBox stc = (MathBox) Proxy.newProxyInstance(
                SetMathBoxHandler.class.getClassLoader(),
                new Class[]{MathBox.class},
                mathBoxHandler);

        System.out.println("Содержимое коллекции Set");
        System.out.println(myMathBox.toString());
        System.out.println("");
        System.out.println("Without proxy:");
        System.out.println("Деление всех элементов коллекции");
        myMathBox.getSplit(2);
        System.out.println("Умножение всех элементов коллекции");
        myMathBox.getMultiplied(2);
        System.out.println("Содержимое коллекции Set");
        System.out.println(myMathBox.toString());
        System.out.println("");
        System.out.println("With proxy:");
        System.out.println("Деление всех элементов коллекции");
        stc.getSplit(2);
        System.out.println("Умножение всех элементов коллекции");
        stc.getMultiplied(2);
        System.out.println("Содержимое коллекции Set");
        System.out.println(myMathBox.toString());



    }
}
