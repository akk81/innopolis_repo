package ru.kulikov.task8;

import java.util.Map;

public interface MathBox {
    public void add(Double number);

    public void remove(Double number);

    public double getTotal();

    public void getSplit(int splitter);

    public double getAverage();

    public void getMultiplied(int multiplicator);

    public double getMax();

    public double getMin();

    public Map<Integer, String> getMap();
}
