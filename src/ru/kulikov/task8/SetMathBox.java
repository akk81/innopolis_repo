package ru.kulikov.task8;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SetMathBox implements MathBox{

    private Set<Double> mySet;

    public SetMathBox(Number[] array) {
        mySet = new HashSet<>();
        for (Number element: array) {
            mySet.add(element.doubleValue());
        }
    }

    @Override
    public void add(Double number){
        mySet.add(number);
    }

    @Override
    public void remove(Double number){
        mySet.remove(number);
    }

    @Override
    @ClearData
    public double getTotal(){
        double total = 0;
        for (Double element: mySet) {
            total += element;
        }
        return total;
    }

    @Override
    @LogData
    public void getSplit(int splitter){
        Set<Double> tempSet = new HashSet<>();
        double temp;
        for (Double element: mySet) {
            temp = element;
            tempSet.add(temp / splitter);
        }
        mySet.clear();
        mySet.addAll(tempSet);
    }

    @Override
    public double getAverage(){
        return this.getTotal() / mySet.size();
    }

    @Override
    @LogData
    @ClearData
    public void getMultiplied(int multiplicator){
        Set<Double> tempSet = new HashSet<>();
        double temp;
        for (Double element: mySet) {
            temp = element;
            tempSet.add(temp * multiplicator);
        }
        mySet.clear();
        mySet.addAll(tempSet);
    }

    @Override
    public double getMax(){
        double maxNumber = mySet.iterator().next();
        for (Double element: mySet) {
            if (maxNumber < element) {maxNumber = element;}
        }
        return maxNumber;
    }

    @Override
    public double getMin(){
        double minNumber = mySet.iterator().next();
        for (Double element: mySet) {
            if (minNumber > element) {minNumber = element;}
        }
        return minNumber;
    }

    @Override
    public Map<Integer, String> getMap(){
        Map<Integer, String> map = new HashMap<>();
        Set<Integer> tempSet = new HashSet<>();
        for (Double element: mySet) {
            tempSet.add(element.intValue());
        }
        for (Integer element: tempSet) {
            map.put(element, element.toString());
        }
        return map;
    }

    @Override
    public String toString(){
        return mySet.toString();
    }
}
