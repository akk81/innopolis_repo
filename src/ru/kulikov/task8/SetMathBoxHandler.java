package ru.kulikov.task8;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashSet;

public class SetMathBoxHandler implements InvocationHandler {
    private MathBox mathBox;

    public SetMathBoxHandler(MathBox mathBox) {
        this.mathBox = mathBox;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (mathBox.getClass().getMethod(method.getName(), method.getParameterTypes()).isAnnotationPresent(LogData.class)) {
            System.out.println(mathBox);
        }

        Object result = method.invoke(mathBox, args);

        if (mathBox.getClass().getMethod(method.getName(), method.getParameterTypes()).isAnnotationPresent(LogData.class)) {
            System.out.println(mathBox);
        }
        if (mathBox.getClass().getMethod(method.getName(), method.getParameterTypes()).isAnnotationPresent(ClearData.class)) {
            Field mySetField = mathBox.getClass().getDeclaredField("mySet");
            mySetField.setAccessible(true);
            HashSet<Double> mySet = (HashSet<Double>) mySetField.get(mathBox);
            mySet.clear();
            mySetField.setAccessible(false);
        }
        return result;
    }
}
