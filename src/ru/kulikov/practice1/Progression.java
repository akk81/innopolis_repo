package ru.kulikov.practice1;

import java.util.Scanner;

public class Progression {
    public static void main(String[] args) {
        System.out.println("Введите разность/знаменатель прогрессии:");
        Scanner console = new Scanner(System.in);
        if (console.hasNextDouble()) {
            double a = console.nextDouble();
            int b = 1;
            double c;
            System.out.println("Первые десять членов арифметической прогрессии:");
            for (int i = 1; i <= 10; i++) {
                c = b + (i - 1) * a;
                System.out.print(c + "; ");
            }
            System.out.println("");
            System.out.println("Первые десять членов геомерической прогрессии:");
            for (int i = 1; i <= 10; i++) {
                c = b * Math.pow(a, (i - 1));
                System.out.print(c + "; ");
            }
        } else {
            System.out.println("Ошибка!");
        }

    }
}