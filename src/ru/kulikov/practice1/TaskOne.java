package ru.kulikov.practice1;

import java.util.Scanner;

public class TaskOne {
    public static void main(String[] args) {
        int previous = 0;
        int numberTarget = (int) (Math.random() * 100);
        System.out.println(numberTarget);

        Scanner console = new Scanner(System.in);
        do {
            if (console.hasNextInt()) {
                int current = console.nextInt();
                int diffCurrent = Math.abs(current - numberTarget);
                int diffPrevious = Math.abs(previous - numberTarget);
                if (current < 1 || current > 100) {
                    System.out.println("Error!");
                } else if (current == numberTarget) {
                    System.out.println("Success!");
                    break;
                } else if (diffPrevious < diffCurrent) {
                    System.out.println("Cool!");
                } else if (diffPrevious > diffCurrent) {
                    System.out.println("Hot!");
                }
                previous = current;
            } else if (console.hasNext("стоп")) {
                break;
            } else {
                System.out.println("Error!");
            }
        }
        while (true);
        console.close();
    }
}
