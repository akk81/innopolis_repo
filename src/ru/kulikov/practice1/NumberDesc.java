package ru.kulikov.practice1;

import java.util.Scanner;

public class NumberDesc {
    public static void main(String[] args) {
        System.out.println("Введите число:");
        Scanner console = new Scanner(System.in);
        if (console.hasNextDouble()) {
            double a = console.nextDouble();
            if (a == 0) {
                System.out.println("Введено значение 0.");
                //System.exit(0);
            } else if (a < 0) {
                System.out.println("Число отрицательное.");
            } else {
                System.out.println("Число положительное.");
            }
            if (a % 2 == 0) {
                System.out.println("Число четное.");
            } else {
                System.out.println("Число нечетное.");
            }
        } else {
            System.out.println("Ошибка!");
        }

    }
}
