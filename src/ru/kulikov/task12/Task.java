package ru.kulikov.task12;

import java.math.BigInteger;

public class Task implements Runnable{

    private int value;

    public Task(int task) {
        this.value = task;
    }

    @Override
    public void run() {
        BigInteger factorial = BigInteger.valueOf(1);
        for (int i = 1; i <= value; i++) {
            factorial = factorial.multiply(BigInteger.valueOf(i));
        }
        System.out.println("Thread " + Thread.currentThread().getName() + " calculated " + value + "! = " + factorial);
    }
}