package ru.kulikov.task12;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CalculatingService {

    public CalculatingService() {
    }

    public void factorialCalculate (int threadCount, int[] intList) {

        ExecutorService executor = Executors.newFixedThreadPool(threadCount);

        for (int element : intList) {
            executor.execute(new Task(element));
        }
        executor.shutdown();

    }
}