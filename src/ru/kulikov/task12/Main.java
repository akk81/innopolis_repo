package ru.kulikov.task12;

public class Main {

    private static final int THREAD_COUNT = 2;
    private static final int[] FACTORIAL_CALCULATE =  {9, 54, 16, 18, 55, 74, 98, 6, 31, 52, 8, 98, 88, 80, 83, 71, 49};

    public static void main(String[] args) {
        CalculatingService calculatingService = new CalculatingService();
        calculatingService.factorialCalculate(THREAD_COUNT, FACTORIAL_CALCULATE);
    }
}

