package ru.kulikov.task3;

public class EXEP {

    private static void array (int n) {
        int[] arr = new int[n];
        for (int i = 0; i <= arr.length; i++) {
            try {
                arr[i] = 1 + (int) (Math.random() * 100);
                System.out.println(arr[i]);
            } catch (ArrayIndexOutOfBoundsException a){
                System.out.println("Произошла ошибка при заполнении массива. Проверьте условия!");
            }

        }
    }

    public static void main(String args[]) {
        String str = null;
        String error = "Проверка показала что объект null";
        String fin = "Выполняется блок finally";
        //str = new String ("Hello, world!");
        try {
            if (str == null) {
                throw new NullPointerException(error);
            }
            //System.out.println(str.length());
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println(fin);
            array (4);
        }
    }
}
