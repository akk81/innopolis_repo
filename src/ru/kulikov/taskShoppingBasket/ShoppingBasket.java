package ru.kulikov.taskShoppingBasket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShoppingBasket implements Basket {

    private Map<String, Integer> map;

    public ShoppingBasket() {
        this.map = new HashMap<>();
    }

    @Override
    public void addProduct(String product, int quantity) {
        if (quantity > 0) {
            Integer qty = map.get(product);
            map.put(product, qty == null ? quantity : qty + quantity);
        }
    }

    @Override
    public void removeProduct(String product) {
        map.remove(product);
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        if (quantity == 0) {
            removeProduct(product);
        } else if (quantity > 0) {
            map.replace(product, quantity);
        }
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public List<String> getProducts() {
        List<String> list = new ArrayList<>();
        for (Map.Entry<String, Integer> element : map.entrySet()) {
            list.add(element.getKey() + " - " + element.getValue());
        }
        return list;
    }

    @Override
    public int getProductQuantity(String product) {
        return map.get(product);
    }
}
