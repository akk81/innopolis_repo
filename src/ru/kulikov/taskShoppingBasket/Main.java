package ru.kulikov.taskShoppingBasket;

public class Main {

    public static void main(String[] args) {
        Basket basket = new ShoppingBasket();

        basket.addProduct("Товар1", 5);
        basket.addProduct("Товар2", 2);
        basket.addProduct("Товар3", 10);
        basket.addProduct("Товар4", 3);
        basket.addProduct("Товар4", 3);
        basket.addProduct("Товар5", 1);
        basket.addProduct("Товар6", 0);

        System.out.println(basket.getProducts());
        basket.removeProduct("Товар5");
        basket.updateProductQuantity("Товар3", 8);
        System.out.println(basket.getProductQuantity("Товар4"));
        System.out.println(basket.getProducts());
        basket.clear();
        System.out.println(basket.getProducts());
    }
}
