package ru.kulikov.task2.vending.drinks;

public interface Drink {
    double getPrice();
    String getTitle();
}
