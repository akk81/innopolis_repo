package ru.kulikov.task2.vending;

import ru.kulikov.task2.vending.drinks.ColdDrink;
import ru.kulikov.task2.vending.drinks.Drink;
import ru.kulikov.task2.vending.drinks.HotDrink;

import java.util.Scanner;

public class Main {

    public enum  Item{
        TEE("Чай", 150), COLA("Кола", 50);

        String title;
        double price;

        private Item (String title, double price){
            this.title = title;
            this.price = price;
        }
    }

    public static void main(String[] args) {

        //Drink[] hotDrinks = new HotDrink[]{new HotDrink("Чай", 150)};
        //Drink[] coldDrinks = new ColdDrink[]{new ColdDrink("Кола", 50)};

        Drink[] hotDrinks = new HotDrink[]{new HotDrink(Item.TEE.title, Item.TEE.price)};
        Drink[] coldDrinks = new ColdDrink[]{new ColdDrink(Item.COLA.title, Item.COLA.price)};

        VendingMachine vm = new VendingMachine(hotDrinks);

        System.out.println("Выберите автомат:");
        System.out.println("1 - горячие напитки.");
        System.out.println("2 - холодные напитки.");
        Scanner console = new Scanner(System.in);

        switch (console.next()){
            case "1": break;
            case "2":
                vm.setDrinks(coldDrinks);
                break;
            default:
                System.out.println("Ошибка!");
                System.exit(0);
        }

        do {
            System.out.println("Выберите действие:");
            System.out.println("1 - внести оплату" + "(Внесено - " + (int)vm.giveMeADeposit() + "р.)");
            System.out.println("2 - получить напиток");
            System.out.println("Другое значение - выход");

            switch (console.next()) {
                case "1":
                    System.out.println("Введите сумму");
                    if (console.hasNextInt()){
                        vm.addMoney(console.nextInt());
                    } else {
                        System.out.println("Ошибка");
                        System.exit(0);
                    }
                    break;
                case "2":
                    vm.giveMeADrink(0);
                    break;
                default:
                    System.out.println("Спасибо за покупку!");
                    System.exit(0);
            }
        }
        while (true);

        /*
        vm.addMoney(200);
        vm.giveMeADrink(0);

        vm.setDrinks(coldDrinks);
        vm.giveMeADrink(0);
        */

    }

}
