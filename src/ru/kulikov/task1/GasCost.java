package ru.kulikov.task1;

import java.util.Scanner;

public class GasCost {

    public static void main (String[] args) {
        System.out.println("Введите количество литров:");
        Scanner console = new Scanner(System.in);
        int vol = console.nextInt();
        validate(vol);
        int cost = vol * 43;
        System.out.println("Стоимость бензина " + cost);
    }

    private static void validate ( int console) {
        if (console < 0) {
            System.out.println("Ошибка");
            System.exit(0);
        }
    }
}
