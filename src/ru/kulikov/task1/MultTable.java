package ru.kulikov.task1;

import java.util.Scanner;

public class MultTable {

    public static void main (String[] args) {
        System.out.println("Введите разряд таблицы:");
        Scanner console = new Scanner(System.in);
        int a = console.nextInt();
        for (int i = 1; i <= a; i++) {
            for (int j = 1; j <= a; j++) {
                System.out.printf("%4d", i * j);
            }
            System.out.println();
        }
    }
}
