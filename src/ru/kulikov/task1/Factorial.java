package ru.kulikov.task1;

public class Factorial {

    private static long fact (int n) {
        long fact = 1;
        for (int i = 1; i <= n; i ++){
            fact = fact*i;
        }
        return fact;
      }

    public static void main (String[] args) {
        int n = Integer.parseInt(args[0].trim());
        System.out.println(n +"! = " + fact(n));
    }
}
