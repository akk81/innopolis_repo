package ru.kulikov.task1;

public class MinValue {

    public static void main (String[] args) {
        int a = Integer.parseInt(args[0].trim());
        int b = Integer.parseInt(args[1].trim());
        int c = Integer.parseInt(args[2].trim());
        int d = Integer.parseInt(args[3].trim());
        int min = Math.min(Math.min(a,b),Math.min(c,d));
        System.out.println("Наименьшее число " + min);
    }

}
